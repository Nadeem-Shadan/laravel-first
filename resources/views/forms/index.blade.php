@extends('layouts.app')
@section('content')
<div id="app">
    <welcome></welcome>
    @if(!Auth::user())
    <div class="jumbotron text-center">
        <h1>{{ config('app.name', 'Fizzy Bios') }} &nbsp; <i class="glyphicon glyphicon-book"></i></h1>
        <p><a class="btn btn-primary" href="/login">Sign in</a> <a href="/register" class="btn btn-success">Register</a></p>
        <header> or view bios if you wish.. <a href="/trees" class="btn-xs btn btn-default">View</a></header>
    @else
        <h1>
            <h1>{{ config('app.name', 'Fizzy Bios') }} &nbsp; <i class="glyphicon glyphicon-book"></i></h1>
            <a href="/trees" class="btn-lg btn btn-warning">View Bios</a><br><br>
            <a href="/trees/create" class="btn-lg btn btn-info">Create Bio</a>

        </h1>
@endif
    </div>
</div>
@endsection
