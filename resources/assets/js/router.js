import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes=new VueRouter({
  routes: [{path: '/',
      name: 'test',
      component: require('./components/welcome')
    },{
      path: '/addMoto',                                         
      name: 'addBike',
      component: require('./components/listBios')
  },
  {
    path: '/login',
    name: 'login',
    component: login
},
  {
    path: '/register',
    name: 'register',
    component: register
}, {
    path: '/bios',
    name: 'bios',
    component: bios,
}

  ],
linkActiveClass: 'active'
})

export default router
