import Vue from 'vue';
// import router from './router'
import welcome from './components/welcome'

require('./bootstrap');
Vue.component('example', require('./components/Example.vue'));
Vue.component('bios', require('./components/listBios.vue'));

const app = new Vue({
    el: '#app'
});
